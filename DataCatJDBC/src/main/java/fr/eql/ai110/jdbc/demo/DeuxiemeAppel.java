package fr.eql.ai110.jdbc.demo;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

public class DeuxiemeAppel {

	public static void main(String[] args) {
		
		DataSource ds = new CatDataSource();
		Connection cnx = null;
		
		try {
			cnx = ds.getConnection();
			System.out.println("connexion ok");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				cnx.close();
				System.out.println("connexion fermée");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}
